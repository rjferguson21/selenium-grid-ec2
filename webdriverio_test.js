var webdriverio = require('webdriverio');
var platform = 'WINDOWS';
var browsers = ['firefox', 'chrome', 'ie8', 'ie9', 'ie10', 'ie11'];
var client, version, capabilities;
browsers.map(function(browserName) {
    capabilities = {
        host: 'localhost',
        port: 4444,
        desiredCapabilities: {
            platform: platform,
            initialBrowserUrl: ''
        }
    };
	switch(browserName) {
	case 'ie8':
	    browserName = 'internet explorer';
		capabilities.desiredCapabilities['ie.forceCreateProcessApi'] = true;
		capabilities.desiredCapabilities['ie.browserCommandLineSwitches'] = "-private";
	    version = '8';
	    break;
	case 'ie9':
	    browserName = 'internet explorer';
	    capabilities.desiredCapabilities['ie.forceCreateProcessApi'] = true;
		capabilities.desiredCapabilities['ie.browserCommandLineSwitches'] = "-private";
	    version = '9';
	    break;
	case 'ie10':
	    browserName = 'internet explorer';
	    capabilities.desiredCapabilities['ie.forceCreateProcessApi'] = true;
		capabilities.desiredCapabilities['ie.browserCommandLineSwitches'] = "-private";
	    version = '10';
	    break;
	case 'ie11':
	    browserName = 'internet explorer';
	    capabilities.desiredCapabilities['ie.forceCreateProcessApi'] = true;
		capabilities.desiredCapabilities['ie.browserCommandLineSwitches'] = "-private";
	    version = '11';
	    break;
	default:
	    version = '';
    }
	capabilities.desiredCapabilities['browserName'] = browserName;
	capabilities.desiredCapabilities['version'] = version;
    client = webdriverio.remote(capabilities);
    client.init();
    client.url('https://whatbrowser.org/').getText('.browser-icon', function(err, browserVersion) {
            console.log(browserVersion[0]);
    });
    client.end();
});